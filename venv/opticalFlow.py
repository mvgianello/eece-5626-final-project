import numpy as np
import cv2 as cv

# https://github.com/opencv/opencv/blob/master/samples/python/lk_homography.py
def draw_str(dst, target, s):
    x, y = target
    cv.putText(dst, s, (x+1, y+1), cv.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 0), thickness = 2, lineType=cv.LINE_AA)
    cv.putText(dst, s, (x, y), cv.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), lineType=cv.LINE_AA)

feature_params = dict(maxCorners = 200,
                      qualityLevel = 0.1,
                      minDistance = 20,
                      blockSize = 3)

lk_params = dict(winSize=(15, 15),
                 maxLevel=4,
                 criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.3))

def checkedTrace(img0, img1, p0, back_threshold = 1.0):
    p1, _st, _err = cv.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
    p0r, _st, _err = cv.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
    d = abs(p0-p0r).reshape(-1, 2).max(-1)
    status = d < back_threshold
    return p1, status

green = (0, 255, 0)
red = (0, 0, 255)

class App2:
    def __init__(self, capture):
        self.cam = capture
        self.p0 = None
        self.use_ransac = True
        self.frame0 = None
        self.p1 = None
        self.gray1 = None

    def run(self, frame_gray, vis):
        overlay = None
        if self.p0 is not None:
            flow, trace_status = checkedTrace(self.gray1, frame_gray, self.p1)

            self.p1 = flow[trace_status].copy()
            self.p0 = self.p0[trace_status].copy()
            self.gray1 = frame_gray

            if len(self.p0) < 4:
                self.p0 = None
                return None

            H, status = cv.findHomography(self.p0, self.p1, (0, cv.RANSAC)[self.use_ransac], 10.0)
            h, w = vis.shape[:2]
            if H.any():
                overlay = cv.warpPerspective(self.frame0, H, (w, h))

            for (x0, y0), (x1, y1), good in zip(self.p0[:, 0], self.p1[:, 0], status[:, 0]):
                if good:
                    cv.line(vis, (x0, y0), (x1, y1), (0, 128, 0))
                cv.circle(vis, (x1, y1), 2, (red, green)[good], -1)
            draw_str(vis, (20, 20), 'track count: %d' % len(self.p1))
            if self.use_ransac:
                draw_str(vis, (20, 40), 'RANSAC')

        else:
            p = cv.goodFeaturesToTrack(frame_gray, **feature_params)
            if p is not None:
                for x, y in p[:, 0]:
                    cv.circle(vis, (x, y), 2, green, -1)
                draw_str(vis, (20, 20), 'feature count: %d' % len(p))

        self.frame0 = vis
        self.p0 = cv.goodFeaturesToTrack(frame_gray, **feature_params)
        if self.p0 is not None:
            self.p1 = self.p0
            self.gray1 = frame_gray

        #cv.imshow('lk_homography', vis)


        return overlay