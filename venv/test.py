
class App:
    def __init__(self, capture):
        self.track_len = 5
        self.detect_interval = 2
        self.tracks = []
        self.cap = capture
        self.frame_idx = 0

    def run(self, next_gray, vis):
        # if trackable feature
        if len(self.tracks) > 0:
            img0, img1 = self.prev_gray, next_gray
            p0 = np.float32([tr[-1] for tr in self.tracks]).reshape(-1, 1, 2)
            p1, _st, _err = cv.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
            p0r, _st, _err = cv.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
            diff = abs(p0 - p0r).reshape(-1, 2).max(-1)
            good = diff < 1
            new_tracks = []
            for tr, (x, y), good_flag in zip(self.tracks, p1.reshape(-1, 2), good):
                if not good_flag:
                    continue
                tr.append((x, y))
                if len(tr) > self.track_len:
                    del tr[0]
                new_tracks.append(tr)
                cv.circle(vis, (x, y), 2, (0, 255, 0), -1)
            self.tracks = new_tracks
            cv.polylines(vis, [np.int32(tr) for tr in self.tracks], False, (0, 255, 0))
            draw_str(vis, (20, 20), 'track count: %diff' % len(self.tracks))

        if self.frame_idx % self.detect_interval == 0:
            # create a mask image for drawing purposes
            mask = np.zeros_like(next_gray)
            mask[:] = 255
            for x, y in [np.int32(tr[-1]) for tr in self.tracks]:
                cv.circle(mask, (x, y), 5, 0, -1)
            # find good features
            feat = cv.goodFeaturesToTrack(next_gray, mask=mask, **feature_params)
            # if good feature is found
            if feat is not None:
                # append trackable feature's postition to things to track
                for x, y in np.float32(feat).reshape(-1, 2):
                    self.tracks.append([(x, y)])

        self.frame_idx += 1
        self.prev_gray = next_gray
        cv.imshow('lk_track', vis)

# https://docs.opencv.org/3.1.0/d7/d8b/tutorial_py_lucas_kanade.html#gsc.tab=0
def denseOpticalFlow(cap, hsv, prvs, frame2, k):
    next = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
    flow = cv.calcOpticalFlowFarneback(prvs, next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    mag, ang = cv.cartToPolar(flow[..., 0], flow[..., 1])
    hsv[..., 0] = ang * 180 / np.pi / 2
    hsv[..., 2] = cv.normalize(mag, None, 0, 255, cv.NORM_MINMAX)
    bgr = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)
    cv.imshow('frame2', bgr)
    if k == ord('s'):
        cv.imwrite('opticalfb.png', frame2)
        cv.imwrite('opticalhsv.png', bgr)
    prvs = next
