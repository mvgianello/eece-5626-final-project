from __future__ import print_function

import cv2 as cv
import numpy as np

from opticalFlow import App2

import sys

# https://docs.opencv.org/3.1.0/db/d5c/tutorial_py_bg_subtraction.html
def bgs(cap, frame, fgbg):
    fgmask = fgbg.apply(frame)

    frame_out = cv.resize(fgmask, (int(fgmask.shape[1] / 2), int(fgmask.shape[0] / 2)))

    cv.imshow('frame', frame_out)

def fixBorder(frame):
    s = frame.shape
    # Scale the image 4% without moving the center
    T = cv.getRotationMatrix2D((s[1] / 2, s[0] / 2), 0, 1.04)
    frame = cv.warpAffine(frame, T, (s[1], s[0]))
    return frame


feature_params = dict(maxCorners = 400,
                      qualityLevel = 0.3,
                      minDistance = 40,
                      blockSize = 3)

lk_params = dict(winSize=(15, 15),
                 maxLevel=5,
                 criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.3))

def draw_str(dst, target, s):
    x, y = target
    cv.putText(dst, s, (x+1, y+1), cv.FONT_HERSHEY_PLAIN, 1.0, (0, 0, 0), thickness = 2, lineType=cv.LINE_AA)
    cv.putText(dst, s, (x, y), cv.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), lineType=cv.LINE_AA)

green = (0, 255, 0)
red = (0, 0, 255)

def checkedTrace(img0, img1, p0, tracks, back_threshold = 1.0):
    p1, _st, _err = cv.calcOpticalFlowPyrLK(img0, img1, p0, None, **lk_params)
    p0r, _st, _err = cv.calcOpticalFlowPyrLK(img1, img0, p1, None, **lk_params)
    d = abs(p0-p0r).reshape(-1, 2).max(-1)
    status = d < back_threshold
    p1 = p1[status]
    p0 = p0[status]
    new_track = []
    for tr, (x, y), good_flag in zip(tracks, p1.reshape(-1, 2), status):
        if not good_flag:
            continue
        tr.append((x, y))
        if len(tr) > len(tracks):
            del tr[0]
        new_track.append(tr)
    return new_track, p0, p1

def main():
    try:
        video_src = '/home/choro/Downloads/20190214_112901_Trim2.mp4'
        #sys.argv[1]
    except:
        print('missing input')

    cap = cv.VideoCapture(video_src)
    _ret, frame1 = cap.read()
    frame_gray1 = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
    frame1 = cv.resize(frame1, (int(frame1.shape[1] / 5), int(frame1.shape[0] / 5)))
    frame_gray1 = cv.resize(frame_gray1, (int(frame_gray1.shape[1] / 5), int(frame_gray1.shape[0] / 5)))
    vis1 = frame1.copy()

    #write
    h1, w1 = frame1.shape[:2]
    outVid = cv.VideoWriter("out", cv.VideoWriter_fourcc(*'MJPG'), int(cap.get(cv.CAP_PROP_FPS)), (w1, h1), True)
    outVid.write(frame1)
    tracks = []
    frame_idx = 0
    detect_interval = 4
    outFrame = None
    while(cap.isOpened()):
        _ret2, frame2 = cap.read()
        frame_gray2 = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
        frame2 = fixBorder(cv.resize(frame2, (int(frame2.shape[1] / 5), int(frame2.shape[0] / 5))))
        frame_gray2 = fixBorder(cv.resize(frame_gray2, (int(frame_gray2.shape[1] / 5), int(frame_gray2.shape[0] / 5))))
        vis2 = frame2.copy()

        # help with illumination
        frame_gray1 = cv.bilateralFilter(frame_gray1, 5, 35, 75)
        frame_gray2 = cv.bilateralFilter(frame_gray2, 5, 35, 75)

        #if found good features to track
        if len(tracks) > 0:
            #dispay them
            for x, y in feat[:, 0]:
                cv.circle(vis1, (x, y), 2, green, -1)
            draw_str(vis1, (20, 20), 'feature count: %d' % len(feat))
            # remove outliers
            new_track, p0, p1 = checkedTrace(frame_gray1, frame_gray2, feat, tracks)
            mag, ang = cv.cartToPolar(p1[..., 0], p1[..., 1])

            H, status = cv.findHomography(p0, p1, (0, cv.RANSAC)[True], 10.0)
            h, w = vis1.shape[:2]

            if H is not None:
                outFrame = cv.warpPerspective(frame_gray2, H, (w,h))

        if frame_idx % detect_interval == 0:
            mask = np.zeros_like(frame_gray2)
            mask[:] = 255
            for x, y in [np.int32(tr[-1]) for tr in tracks]:
                cv.circle(mask, (x, y), 5, 0, -1)
            # find good features
            feat = cv.goodFeaturesToTrack(frame_gray1, mask=mask, **feature_params)
            # if good feature is found
            if feat is not None:
                # append trackable feature's postition to track
                for x, y in np.float32(feat).reshape(-1, 2):
                    tracks.append([(x, y)])

        if outFrame is not None:
            outFrame = fixBorder(outFrame)
            outVid.write(outFrame)
            thresh = np.min(mag)
            outFrame = outFrame - frame_gray1
            outFrame[outFrame > thresh] = 0
            cv.imshow("input", vis2)
            cv.imshow("output", outFrame)

        frame1 = frame2
        frame_gray1 = frame_gray2
        vis1 = vis2
        frame_idx = frame_idx + 1

        ch = cv.waitKey(1)
        if ch == 27:
            break

    print('Done')
    cap.release()

if __name__ == '__main__':
    main()
    cv.destroyAllWindows()